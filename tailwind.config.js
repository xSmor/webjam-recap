module.exports = {
  content: ['./src/index.html', './src/js/**/*.js', './src/json/**/*.json'],
  theme: {
    extend: {
      colors: {
        'cabbage-green': '#39AA3D',
      },
      maxWidth: {
        'screen-3xl': '1920px',
      },
      spacing: {
        104: '26rem',
        144: '36rem',
        188: '47rem',
      },
      strokeWidth: {
        6: '6px',
      },
      gap: {
        136: '34rem',
        160: '40rem',
      },
    },
    fontFamily: {
      body: ['Roboto', 'sans-serif'],
    },
  },
  plugins: [],
}
