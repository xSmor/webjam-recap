function randomBetween(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

function padLeadingZeroes(num, size) {
  num = num.toString()
  while (num.length < size) num = '0' + num
  return num
}

function appendCounter(nodeList, counter) {
  for (let i = 0; i < nodeList.length; i++) {
    nodeList[i].textContent = counter[i]
  }
}

function flipAnimation(nodeList, oldCounter, newCounter) {
  for (let i = 0; i < nodeList.length; i++) {
    if (oldCounter[i] !== newCounter[i]) {
      nodeList[i].classList.add('flip-animation')

      setTimeout(() => {
        nodeList[i].classList.remove('flip-animation')
      }, 2200)
    }
  }
}

function counterTick(counter, firstNodeList, secondNodeList) {
  setTimeout(() => {
    if (+counter >= 9999) {
      return
    }

    const calculatedTotal = +counter + randomBetween(2, 5)
    const checkedTotal = calculatedTotal > 9999 ? 9999 : calculatedTotal
    const newCounter = padLeadingZeroes(checkedTotal, 4)

    appendCounter(firstNodeList, newCounter)

    flipAnimation(firstNodeList, counter, newCounter)

    setTimeout(() => {
      appendCounter(secondNodeList, newCounter)
    }, 1700)

    counterTick(newCounter, firstNodeList, secondNodeList)
  }, randomBetween(3000, 6000))
}

export default function () {
  const firstCounterNumbers = document.querySelectorAll('[data-voting-number-first]')
  const secondCounterNumbers = document.querySelectorAll('[data-voting-number-second]')
  const counter = padLeadingZeroes(randomBetween(10, 30), 4)

  if (!firstCounterNumbers || !secondCounterNumbers) {
    return
  }

  appendCounter(secondCounterNumbers, counter)
  counterTick(counter, firstCounterNumbers, secondCounterNumbers)
}
