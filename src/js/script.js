import { initObservers, addTechnologies } from './timeline.js'
import technologies from '../json/technologies.json'
import votingCounter from './voting-counter.js'

votingCounter()
initObservers()
addTechnologies(technologies)
