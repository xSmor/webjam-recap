function initObservers() {
  const timeline = document.getElementById('timeline')

  if (!timeline) {
    return
  }

  const years = timeline.querySelectorAll('[data-year]')

  if (!years) {
    return
  }

  const observer = new IntersectionObserver(
    (entries) => {
      entries.forEach((entry) => {
        const technologies = entry.target.querySelectorAll('[data-technology-wrapper]')

        technologies.forEach((technology) => {
          technology.classList.toggle('opacity-0', !entry.isIntersecting)
          technology.classList.toggle('translate-y-1/2', !entry.isIntersecting)
        })

        const svg = entry.target.querySelector('[data-circle]')
        svg.classList.toggle('fill-cyan-500', entry.isIntersecting)
      })
    },
    {
      root: null,
      rootMargin: '-20% 0% -40% 0%',
      threshold: 0,
    },
  )

  years.forEach((year) => {
    observer.observe(year)
  })
}

function addTechnologies(technologies) {
  const years = timeline.querySelectorAll('[data-year]')

  if (!years) {
    return
  }

  years.forEach((year) => {
    const techForYear = technologies[year.dataset.year]

    const items = techForYear
      .map(
        (tech) =>
          `<div class='2xl:text-3xl text-2xl absolute tooltip z-10 transition duration-300 opacity-0 transform translate-y-1/2 ${tech.wrapperClass} ${tech.user}' data-text="${tech.user}: ${tech.name}" data-technology-wrapper><img src="./images/${tech.iconName}.svg" alt="${tech.iconName}" class="technology ${tech.imgClass}" data-technology /></div>`,
      )
      .join('')

    year.innerHTML += items
  })
}

export { initObservers, addTechnologies }
